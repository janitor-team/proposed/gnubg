gnubg for Debian
----------------

gnubg needs a pre-computed two-sided bearoff database for maximum
strength.  Since this database takes some time to build, particularly on
slow systems, and takes over 6MB of disk space, the default is not to
build it.  If you weren't asked whether you wanted to build the bearoff
database at installation time, or if you've changed your mind, run:

    dpkg-reconfigure gnubg

to be asked again.  (The one-sided bearoff database is much smaller and is
therefore included in the gnubg-data package.)

The Debian version of gnubg has been modified to search in /var/lib/gnubg
as well as /usr/share/gnubg for its data files.  This is where the
automatically built bearoff databases are put.  If you want particularly
strong play and don't mind the disk space and computation time, you can
build even larger databases with:

    makebearoff -t 6x8 -f /var/lib/gnubg/gnubg_ts.bd
    makebearoff -o 10 -f /var/lib/gnubg/gnubg_os.bd

These two databases combined will take 190MB of disk space and may take
several hours to compute.  Alternately, you can download prebuilt bearoff
database from <ftp://ftp.demon.nl/pub/games/gnubg/databases/>.  Save
two-sided databases as gnubg_ts.bd and one-sided databases as gnubg_os.bd
in /var/lib/gnubg.

If you want gnubg to use pre-built hypergammon databases, save them in the
same /var/lib/gnubg directory as hyperN.bd where N is the number of
chequers.  See the man page for makehyper for more details.

The 3D board has been disabled for now, since it does not work with Mesa
21.x or later.  It will be re-enabled once this has been fixed.

SSE optimization has been disabled except on the amd64 architecture since
the SSE instructions are not supported on all platforms that Debian
supports and there is no simple way of shipping different binaries
depending on the processor type.

 -- Russ Allbery <rra@debian.org>, Sun, 20 Nov 2022 09:57:23 -0800
