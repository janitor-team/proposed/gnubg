# Japanese debconf templates translation for gnubg.
# Copyright (C) 2007 Noritada Kobayashi
# This file is distributed under the same license as the gnubg package.
#
msgid ""
msgstr ""
"Project-Id-Version: gnubg (debconf) 0.14.3+20060923-4\n"
"Report-Msgid-Bugs-To: rra@debian.org\n"
"POT-Creation-Date: 2006-12-03 16:38-0800\n"
"PO-Revision-Date: 2007-03-05 21:06+0900\n"
"Last-Translator: Noritada Kobayashi <nori1@dolphin.c.u-tokyo.ac.jp>\n"
"Language-Team: Japanese <debian-japanese@lists.debian.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../gnubg.templates:1001
msgid "Build bearoff database?"
msgstr "ベアオフのデータベースを構築しますか?"

# TRANSLATION-FIXME: "two-sided bearoff database"?
# TRANSLATION-FIXME: "use weaker heuristics for the end of the game"?
#. Type: boolean
#. Description
#: ../gnubg.templates:1001
msgid ""
"For maximum strength, GNU Backgammon needs a two-sided bearoff database, "
"used to evaluate positions in the end-game.  This database takes up 6.6MB of "
"disk space and requires several minutes to generate on a reasonably fast "
"computer.  GNU Backgammon is fully playable without this database, but will "
"use weaker heuristics for the end of the game."
msgstr ""
"GNU Backgammon の長所を最大限に利用するには、終盤に位置の評価に使用される両側"
"ベアオフデータベースが必要となります。このデータベースは 6.6 MB のディスク領"
"域を使用し、かなり速いコンピュータにおいても構築に数分間かかります。このデー"
"タベースがなくても GNU Backgammon は十分に楽しめますが、ゲームの終了のために"
"使用される経験則が弱くなります。"
